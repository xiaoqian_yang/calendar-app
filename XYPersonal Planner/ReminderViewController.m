//
//  ReminderViewController.m
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 20/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import "ReminderViewController.h"
#import "ReminderCell.h"
#import "XYCoreDataStack.h"
#import "CollectionHeaderView.h"
#import "ReminderController.h"
#import "ReminderNavigationController.h"
#import "AudioToolbox/AudioServices.h"
#import "RemindPopUpView.h"


@implementation ReminderViewController{
    SystemSoundID audioEffect;
}

- (id)init {
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    
    return (self = [super initWithCollectionViewLayout:layout]);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.viewType = XYPersonalPlannerRemindStatusNormal;
    
    // prepare the searchBar view
    [self prepareSearchBar];
    
    // prepare collection view contentInset/ContentOffset so searchBar fit at the top
    self.collectionView.contentInset = UIEdgeInsetsMake(self.searchBar.frame.size.height, 0, 0, 0);
    self.collectionView.contentOffset   = CGPointMake(0, -self.searchBar.frame.size.height);
    
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, 5, 8, 5);
    collectionViewLayout.minimumInteritemSpacing = 0;
    
    [self.collectionView registerClass:[ReminderCell class] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.fetchRequestController performFetch:nil];
    self.fetchRequestController.delegate =self;
    
    self.buttonByTime.layer.borderWidth = 0.5;
    self.buttonByTime.layer.borderColor = [[UIColor blackColor] CGColor];
    self.buttonByTime.layer.cornerRadius = 10.0f;
    self.buttonLocation.layer.borderWidth = 0.5;
    self.buttonLocation.layer.borderColor = [[UIColor blackColor] CGColor];
    self.buttonLocation.layer.cornerRadius = 10.0f;
    
    self.popView = [[RemindPopUpView alloc] init];
    NSString *path  = [[NSBundle mainBundle] pathForResource:@"crystal_ball" ofType:@"mp3"];
    NSURL *pathURL = [NSURL fileURLWithPath : path];
    
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
    //[self setToolbar];

    
}

-(void) viewDidAppear:(BOOL)animated {
    [self refreshCollectionView];
    [self.reminderTypeView removeFromSuperview];
}

-(void) viewDidDisappear:(BOOL)animated {
    self.reminderTypeView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger count = [self.fetchRequestController.sections count];
    return count;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchRequestController.sections[section];
    NSInteger count = [sectionInfo numberOfObjects];
    return count;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ReminderCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Reminder * reminder = [self.fetchRequestController objectAtIndexPath:indexPath];
    [cell configCell:reminder];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.viewType isEqual:XYPersonalPlannerRemindStatusFinished]) {
        return;
    }
    [self performSegueWithIdentifier:@"editReminder" sender:[collectionView cellForItemAtIndexPath:indexPath]];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect rx = [ UIScreen mainScreen ].bounds;
    CGSize size = CGSizeMake((rx.size.width-10)/2, 60);
    return size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath

{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader){
        
        CollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        if ([self.fetchRequestController.fetchedObjects count] > 0) {
            Reminder * reminder = [self.fetchRequestController objectAtIndexPath:indexPath];
            
            [headerView configHeader:[reminder month]];
        }
        else {
            [headerView configHeader:@"Empty"];
        }
        
        reusableview = headerView;
        
    }
    
    return reusableview;
    
    
    
}

#pragma mark - IBAction

- (IBAction)changeView:(id)sender {
    if ([self.viewType isEqual: XYPersonalPlannerRemindStatusNormal]) {
        self.viewType = XYPersonalPlannerRemindStatusFinished;
    }
    else {
        self.viewType = XYPersonalPlannerRemindStatusNormal;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %@", self.viewType];
    [self.fetchRequestController.fetchRequest setPredicate:predicate];
    [self.fetchRequestController performFetch:nil];
    [self.collectionView reloadData];
}

- (IBAction)addReminder:(id)sender {
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.buttonLocation.enabled = NO;
        [self.buttonLocation setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    }
    else {
        self.buttonLocation.enabled = YES;
    }
    
    // add reminder type view
    [self.view addSubview:self.reminderTypeView];
    self.reminderTypeView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);

    [UIView animateWithDuration:0.25 animations:^{
        self.reminderTypeView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - Request function

- (NSFetchRequest *) fetchRequest {
    if (_fetchRequest != nil) {
        return _fetchRequest;
    }
    
    _fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Reminder"];
    _fetchRequest.sortDescriptors = @[[[NSSortDescriptor alloc] initWithKey:@"time" ascending:YES]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %@", self.viewType];
    [_fetchRequest setPredicate:predicate];

    
    return _fetchRequest;
}

- (NSFetchedResultsController *) fetchRequestController {
    if (_fetchRequestController != nil) {
        return _fetchRequestController;
    }
    
    _fetchRequestController = [[NSFetchedResultsController alloc] initWithFetchRequest:self.fetchRequest managedObjectContext:[XYCoreDataStack defaultStack].managedObjectContext sectionNameKeyPath:@"month" cacheName:nil];
    
    return _fetchRequestController;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UINavigationController *navi = segue.destinationViewController;
    ReminderController * reminderView =(ReminderController *)[navi topViewController];

    if ([segue.identifier isEqual:@"newTimeReminder"]) {
        
        reminderView.reminderType = XYPersonalPlannerRemindTypeTime;
    }
    else if ([segue.identifier isEqual:@"newLocationReminder"]) {
        
        reminderView.reminderType = XYPersonalPlannerRemindTypeLocation;
    }
    else if ([segue.identifier isEqual:@"editReminder"]) {
        
        NSIndexPath * indexPath = [self.collectionView indexPathForCell:sender];
        reminderView.Reminder = [self.fetchRequestController objectAtIndexPath:indexPath];
        
    }
    
    /*
    else if ([segue.identifier isEqual:@"showReminder"]) {
        UINavigationController *navi = segue.destinationViewController;
        ReminderController * reminderView =(ReminderController *)[navi topViewController];
        
        reminderView.Reminder = sender;
        
    }*/
}

#pragma mark - help function

- (void) showReminder:(NSString *)reminderId {
    int sections = [self.fetchRequestController.sections count];
    for (int i = 0; i < sections; i++) {
        id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchRequestController.sections[i];
        int count = [sectionInfo numberOfObjects];
        for (int j = 0; j < count; j++) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:j inSection:i];
            Reminder * reminder = [self.fetchRequestController objectAtIndexPath:indexPath];
            
            if ([reminderId isEqual:reminder.reminderid]) {
                AudioServicesPlaySystemSound(audioEffect);
                //[self performSegueWithIdentifier:@"showReminder" sender:reminder];
                [self.popView configView:reminder superController:self];
                [self.popView setCenter:CGPointMake(self.view.center.x, 100+self.popView.frame.size.height/2)];
                [self.view addSubview:self.popView];
                return;
            }
        }
    }
}

- (void) setViewType:(NSString *)viewType {
    _viewType = viewType;
    if ([viewType isEqual: XYPersonalPlannerRemindStatusNormal]) {
        self.buttonChangeView.title = @"Finished";
        self.title = @"Undo Tasks";
    }
    else {
        self.buttonChangeView.title = @"Undo";
        self.title = @"Finished Tasks";
    }
}

- (void) refreshCollectionView {
    [self.fetchRequestController performFetch:nil];
    [self.collectionView reloadData];
}

#pragma mark searchbar function

-(void)prepareSearchBar{
    if (!self.searchBar) {
        self.searchBarBoundsY = self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
        self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, self.searchBarBoundsY, [UIScreen mainScreen].bounds.size.width, 44)];
        self.searchBar.searchBarStyle       = UISearchBarStyleMinimal;
        self.searchBar.showsCancelButton    = YES;
        self.searchBar.tintColor            = [UIColor whiteColor];
        self.searchBar.barTintColor         = [UIColor darkGrayColor];
        self.searchBar.delegate             = self;
        self.searchBar.placeholder          = @"search here";
        
        [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor darkGrayColor]];
        
        [self.view addSubview:self.searchBar];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %@", self.viewType];
    [self.fetchRequestController.fetchRequest setPredicate:predicate];
    [self.fetchRequestController performFetch:nil];
    [self.collectionView reloadData];

    [self.searchBar resignFirstResponder];// 放弃第一响应者
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSString * search = [NSString stringWithFormat:@"*%@*",searchBar.text];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (title LIKE[cd] %@)", self.viewType, search];
    [self.fetchRequestController.fetchRequest setPredicate:predicate];
    [self.fetchRequestController performFetch:nil];
    [self.collectionView reloadData];

    [self.searchBar resignFirstResponder];// 放弃第一响应者
}


@end

