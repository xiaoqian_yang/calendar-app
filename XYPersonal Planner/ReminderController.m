//
//  newDiaryController.m
//  XYDiary
//
//  Created by Xiaoqian Yang on 30/10/2014.
//  Copyright (c) 2014 XiaoqianYang. All rights reserved.
//

#import "ReminderController.h"
#import "XYCoreDataStack.h"
#import "Reminder.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>

@interface ReminderController ()

@property (nonatomic) UIImage * remindPhoto;
@property (strong, nonatomic) CLLocationManager * locationManager;

@end

@implementation ReminderController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithReminderId {
    self = [super init];
    if (self) {
        //self.time
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.Reminder) {
        if ([self.reminderType isEqual:XYPersonalPlannerRemindTypeTime]) {
            //set timelabel
            NSTimeInterval timeInterval = [[NSDate date]timeIntervalSince1970];
            self.remindTime = [NSDate dateWithTimeIntervalSince1970:timeInterval + 5*60];
            [self setTimeText];
            
            UITapGestureRecognizer *tapTime=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timePressed:)];
            self.time.userInteractionEnabled=YES;
            [self.time addGestureRecognizer:tapTime];
            
            [self.locationTextField setHidden:YES];
            [self.time setHidden:NO];
        }
        else {
            [self.locationTextField setHidden:NO];
            [self.time setHidden:YES];
            
            //[self setupLocationMonitoring];
        }
        
        [self.buttonComplete setHidden:YES];
    }
    else {
        NSLog(@"self.reminder.remindertype: %@", self.Reminder.reminderType);
        if ([self.Reminder.reminderType isEqual:XYPersonalPlannerRemindTypeTime]) {
            self.remindTime = [NSDate dateWithTimeIntervalSince1970:self.Reminder.time];
            [self setTimeText];
            
            UITapGestureRecognizer *tapTime=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timePressed:)];
            self.time.userInteractionEnabled=YES;
            [self.time addGestureRecognizer:tapTime];
            
            [self.locationTextField setHidden:YES];
            [self.time setHidden:NO];
        }
        else {
            self.locationTextField.text = self.Reminder.locationName;
            
            [self.locationTextField setHidden:NO];
            [self.time setHidden:YES];
            
            self.location = self.Reminder.locationName;
            self.latitude = self.Reminder.latitude;
            self.longtitude = self.Reminder.longitude;

        }
        
        self.reminderType = self.Reminder.reminderType;
        self.remindTitle.text = self.Reminder.title;
        self.message.text = self.Reminder.message;
        if (self.Reminder.picture) {
            self.remindPhoto = [UIImage imageWithData:self.Reminder.picture];
        }
    }
    
    //set note textview
    self.message.layer.borderWidth = 0.5;
    self.message.layer.borderColor = [[UIColor grayColor] CGColor];
    
    //set title textfield
    self.remindTitle.layer.masksToBounds = YES;
    self.remindTitle.layer.borderWidth = 0.5;
    self.remindTitle.layer.borderColor = [[UIColor grayColor] CGColor];
    self.remindTitle.layer.cornerRadius = 1;
    
    self.mainImage.imageView.contentMode = UIViewContentModeScaleAspectFill;

    //setTitle
    self.title = @"Task";
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

#pragma mark - IBAction function

- (IBAction)cancel:(id)sender {
    [self dismissSelf];
}

- (IBAction)save:(id)sender {
    if ([self.reminderType isEqual: XYPersonalPlannerRemindTypeLocation]) {
        if (self.location == nil || [self.location length] == 0) {
            [self alert:@"Make sure you have entered Location" withTitle:@"Oops!"];
            return;
        }
    }
    NSString * remindTitle = [[self.remindTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    if ([remindTitle length] == 0) {
        [self alert:@"Make sure you have entered Title" withTitle:@"Oops!"];
        return;
    }
    
    if (self.Reminder) {
        [self updateReminder];
    }
    else {
        [self insertReminder];
    }
    
    [self setNotification];

    if ([self.reminderType isEqual:XYPersonalPlannerRemindTypeTime]) {
        [self dismissSelf];
    }
}

- (void)timePressed:(UITapGestureRecognizer *)gesture {
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.minuteInterval = 1;
    self.datePicker.date = self.remindTime;
    self.datePicker.minimumDate = [NSDate date];
    [self.dateView setFrame:CGRectMake(0, (self.view.frame.size.height - 300)/2, self.dateView.frame.size.width, self.dateView.frame.size.height)];
    self.dateView.layer.borderWidth = 0.5;
    self.dateView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    [self.view addSubview:self.dateView];
}

- (IBAction)cancelDatePicker:(id)sender {
    [self.dateView removeFromSuperview];
}

- (IBAction)confirmDatePicker:(id)sender {
    self.remindTime = self.datePicker.date;
    [self setTimeText];
    
    [self.dateView removeFromSuperview];
    
}

- (IBAction)setComplete:(id)sender {
    XYCoreDataStack * coreDataStack = [XYCoreDataStack defaultStack];
    self.Reminder.status = XYPersonalPlannerRemindStatusFinished;
    [coreDataStack saveContext];
    
    [self removeNotification];
    [self dismissSelf];
}

- (IBAction)imagePressed:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self showChooseSourcetype];
    }
    else {
        [self showPhotoLibrary];
    }
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.message resignFirstResponder];
    [self.remindTitle resignFirstResponder];
}

#pragma mark - Help function

- (void) dismissSelf {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void) updateReminder {
    XYCoreDataStack * coreDataStack = [XYCoreDataStack defaultStack];
    
    if ([self.Reminder.reminderType isEqual:XYPersonalPlannerRemindTypeTime]) {
        NSTimeInterval timeInterval = [self.remindTime timeIntervalSince1970];
        self.Reminder.time = timeInterval;
    }
    else {
        self.Reminder.locationName = self.location;
        self.Reminder.latitude = self.latitude;
        self.Reminder.longitude = self.longtitude;
    }

    self.Reminder.title = self.remindTitle.text;
    self.Reminder.picture = UIImagePNGRepresentation(self.remindPhoto);
    self.Reminder.message = self.message.text;
    
    [coreDataStack saveContext];
}

- (void) insertReminder {
    XYCoreDataStack * coreDataStack = [XYCoreDataStack defaultStack];
    Reminder * reminder = [NSEntityDescription insertNewObjectForEntityForName:@"Reminder" inManagedObjectContext:coreDataStack.managedObjectContext];
    
    reminder.reminderType = self.reminderType;
    if ([reminder.reminderType isEqual: XYPersonalPlannerRemindTypeTime]) {
        NSTimeInterval timeInterval = [self.remindTime timeIntervalSince1970];
        reminder.time = timeInterval;
    }
    else {
        reminder.locationName = self.location;
        reminder.latitude = self.latitude;
        reminder.longitude = self.longtitude;
    }
    reminder.title = self.remindTitle.text;
    reminder.picture = UIImagePNGRepresentation(self.remindPhoto);
    reminder.message = self.message.text;
    reminder.status = XYPersonalPlannerRemindStatusNormal;
    
    reminder.reminderid = [self generateReminderId];
    self.reminderId = reminder.reminderid;
    [coreDataStack saveContext];
}

- (void) showChooseSourcetype {
    UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image From" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo album", nil];
    
    [actionSheet showInView:self.view];
}

- (void) showCamera {
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void) showPhotoLibrary {
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void) loadLocation {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = 1000;
    
    [self.locationManager startUpdatingLocation];
}

-(void)setTimeText {
    NSDateFormatter * fomatter = [[NSDateFormatter alloc] init];
    fomatter.dateStyle = kCFDateFormatterShortStyle;
    fomatter.timeStyle = kCFDateFormatterShortStyle;
    fomatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_AU"];
    [self.time setText:[fomatter stringFromDate:self.remindTime]];
    NSLog(@"setTimeText........remindTime:%@, timeLabel:%@",self.remindTime, self.time.text);
    
    self.remindTime = [fomatter dateFromString:self.time.text];
    self.datePicker.date = self.remindTime;
    NSLog(@"setTimeText........remindTime after:%@",self.remindTime);
}

- (void) setRemindPhoto:(UIImage *)remindImage {
    if (remindImage) {
        _remindPhoto = remindImage;
        self.mainImage.imageView.image = remindImage;
        [self.mainImage setImage:remindImage forState:UIControlStateNormal];
    }
    else {
        self.mainImage.imageView.image = [UIImage imageNamed:@"icn_noimage"];
    }
}

-(void) removeNotification {
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        if (self.Reminder != nil) {
            for (UILocalNotification *noti in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                NSString *notiID = noti.userInfo[XYPersonalPlannerRemindID];
                if ([notiID isEqualToString:self.Reminder.reminderid]) {
                    [[UIApplication sharedApplication] cancelLocalNotification:noti];
                    return;
                }
            }
        }
    }
}

-(void) setNotification {
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls != nil) {
        if (self.Reminder != nil) {
            for (UILocalNotification *noti in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                NSString *notiID = noti.userInfo[XYPersonalPlannerRemindID];
                if ([notiID isEqualToString:self.Reminder.reminderid]) {
                    [[UIApplication sharedApplication] cancelLocalNotification:noti];
                    break;
                }
            }
        }
        
        if ([self.reminderType isEqual: XYPersonalPlannerRemindTypeTime]) {
            UILocalNotification *notif = [[cls alloc] init];
            notif.fireDate = [self.datePicker date];
            notif.timeZone = [NSTimeZone defaultTimeZone];
            
            notif.alertBody = self.remindTitle.text;
            notif.soundName = UILocalNotificationDefaultSoundName;
            notif.applicationIconBadgeNumber += 1;
            
            if (self.Reminder != nil) {
                self.reminderId = self.Reminder.reminderid;
            }
            NSDictionary *userDict = [NSDictionary dictionaryWithObject:self.reminderId
                                                                 forKey:XYPersonalPlannerRemindID];
            notif.userInfo = userDict;
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        }
        else {
            [self setupLocationMonitoring];
        }
        
        /*
        UILocalNotification *not2 = [[cls alloc] init];
        CLLocation * location = [[CLLocation alloc]initWithLatitude:-33.812551 longitude:151.189059];
        not2.region = [[CLRegion alloc] initCircularRegionWithCenter:location.coordinate radius:100 identifier:@""];
        
        not2.regionTriggersOnce = YES;
        not2.alertBody = @"This is from location";
        not2.alertAction = @"Show me";
        not2.soundName = UILocalNotificationDefaultSoundName;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:not2];*/
    }
}

-(NSString *) generateReminderId {
    NSMutableString * reminderId = [[NSMutableString alloc]init];
    NSDateFormatter * fomatter = [[NSDateFormatter alloc] init];
    fomatter.dateStyle = kCFDateFormatterShortStyle;
    fomatter.timeStyle = kCFDateFormatterShortStyle;
    fomatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_AU"];
    
    [reminderId appendString:@"Irene"];
    [reminderId appendString:[fomatter stringFromDate:[NSDate date]]];
    [reminderId appendString:self.remindTitle.text];
    
    return reminderId;
}

- (void) alert:(NSString*)message withTitle:(NSString*)title
{
    UIAlertView * alerview = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alerview show];
}

- (UIImage *)resizeImage:(UIImage*)image width:(float)width height:(float)height {
    CGSize  size = CGSizeMake(width, height);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:rect];
    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resizedImage;
}


#pragma mark - delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self showCamera];
    }
    else if (buttonIndex == 1){
        [self showPhotoLibrary];
    }
    else {
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (image) {
        int size = MIN((image.size.height/568)*2, (image.size.width/320)*2);
        self.remindPhoto = [self resizeImage:image width:image.size.width/size height:image.size.height/size];
    }
    [picker dismissViewControllerAnimated:NO completion:nil];
}

/*
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [self.locationManager stopUpdatingLocation];
    CLLocation * location = [locations firstObject];
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    self.cllocation = [locations firstObject];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark * placeMark = [placemarks firstObject];
        self.location = placeMark.name;
        self.cllocation = placeMark.location;
    }];
}
 */

# pragma location notification

- (void) setupLocationMonitoring
{
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
    }
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 10; // meters
    
    // iOS 8+ request authorization to track the user’s location
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

- (void)registerLocationNotification
{
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    
    CLLocationCoordinate2D remindLocation = CLLocationCoordinate2DMake(self.latitude, self.longtitude);
    if ([notif respondsToSelector:@selector(setRegion:)]) {
        notif.region = [[CLCircularRegion alloc] initWithCenter:remindLocation radius:50 identifier:@"hello"];
    }
    else {
        [self alert:@"Please update iOS to iOS8+ to use location notification" withTitle:@"Ops"];
        [self dismissSelf];
    }
    
    notif.alertBody = self.remindTitle.text;
    notif.regionTriggersOnce = NO;
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.applicationIconBadgeNumber += 1;
    
    if (self.Reminder != nil) {
        self.reminderId = self.Reminder.reminderid;
    }
    NSDictionary *userDict = [NSDictionary dictionaryWithObject:self.reminderId
                                                         forKey:XYPersonalPlannerRemindID];
    notif.userInfo = userDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    /*
    
    UILocalNotification *locNotification = [[UILocalNotification alloc] init];
    locNotification.alertBody = @"Hello again!";
    
    // doesn't work
    locNotification.regionTriggersOnce = NO;
    CLLocationCoordinate2D remindLocation = CLLocationCoordinate2DMake(-33.812621, 151.188750);
    locNotification.region = [[CLCircularRegion alloc] initWithCenter:remindLocation radius:50 identifier:@"PlaceName"];
    
    // works:
    //    locNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
    
    UIApplication *application = [UIApplication sharedApplication];
    [application cancelAllLocalNotifications];
    [application scheduleLocalNotification:locNotification];*/

}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    // check status to see if we’re authorized
    BOOL canUseLocationNotifications = (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedAlways);
    if (canUseLocationNotifications) {
        [self registerLocationNotification];
    }
    
    if (status != kCLAuthorizationStatusNotDetermined) {
        [self dismissSelf];
    }
}


- (IBAction)locationInputDone:(id)sender {
    NSString * locationName = self.locationTextField.text;
    if ([locationName length] == 0) {
        [self alert:@"Please enter a valid place" withTitle:@"Oops!"];
        self.location = nil;
        self.latitude = 0;
        self.longtitude = 0;
        return;
    }
    
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:locationName
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (error) {
                         [self alert:@"Please enter a valid place Or check your network" withTitle:@"Ops!"];
                         self.location = nil;
                         self.latitude = 0;
                         self.longtitude = 0;
                     }
                     else
                     {
                         CLPlacemark *aPlacemark = [placemarks lastObject];
                         NSLog(@"placemark %f, %f", aPlacemark.location.coordinate.latitude, aPlacemark.location.coordinate.longitude);
                         NSMutableString * address = [[NSMutableString alloc] init];
                         if (aPlacemark.subThoroughfare) {
                             [address appendString:aPlacemark.subThoroughfare];
                             [address appendString:@" "];
                         }
                         if (aPlacemark.thoroughfare) {
                             [address appendString:aPlacemark.thoroughfare];
                             [address appendString:@", "];
                         }
                         if (aPlacemark.locality) {
                             [address appendString:aPlacemark.locality];
                             [address appendString:@" "];
                         }
                         if (aPlacemark.administrativeArea) {
                             [address appendString:aPlacemark.administrativeArea];
                             [address appendString:@", "];
                         }
                         if (aPlacemark.country) {
                             [address appendString:aPlacemark.country];
                         }
                         
                         self.locationTextField.text = address;
                         self.location = address;
                         self.latitude = aPlacemark.location.coordinate.latitude;
                         self.longtitude = aPlacemark.location.coordinate.longitude;
                    }
                 }];
}
@end
