//
//  ReminderNavigationController.m
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 22/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import "ReminderNavigationController.h"

@interface ReminderNavigationController ()

@end

@implementation ReminderNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
