//
//  Reminder.h
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 23/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

static NSString *XYPersonalPlannerRemindStatusFinished = @"com.XYPersonPlanner.RemindStatusFinished";
static NSString *XYPersonalPlannerRemindStatusNormal = @"com.XYPersonPlanner.RemindStatusNormal";
static NSString *XYPersonalPlannerRemindTypeTime = @"com.XYPersonPlanner.RemindTypeTime";
static NSString *XYPersonalPlannerRemindTypeLocation = @"com.XYPersonPlanner.RemindTypeLocation";

@interface Reminder : NSManagedObject

@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSData * picture;
@property (nonatomic, retain) NSString * reminderType;
@property (nonatomic) NSTimeInterval time;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * reminderid;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * locationName;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

//section
@property (nonatomic, retain) NSString * month;

@end
