//
//  ReminderViewController.h
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 20/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
@class RemindPopUpView;
@interface ReminderViewController : UICollectionViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate,UISearchBarDelegate>
@property (strong, nonatomic) NSString * viewType;
@property (strong, nonatomic) NSFetchRequest * fetchRequest;
@property (strong, nonatomic) NSFetchedResultsController * fetchRequestController;
@property (strong, nonatomic) IBOutlet UIView *reminderTypeView;
@property (strong, nonatomic) IBOutlet UIButton *buttonByTime;
@property (strong, nonatomic) IBOutlet UIButton *buttonLocation;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonChangeView;
- (IBAction)changeView:(id)sender;
- (IBAction)addReminder:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIView *searchView2;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField2;

@property (strong, nonatomic) NSString * reminderId;
@property (strong, nonatomic) RemindPopUpView *popView;
@property (nonatomic,strong) UISearchBar    *searchBar;
@property (nonatomic)        BOOL           searchBarActive;
@property (nonatomic)        float          searchBarBoundsY;


- (void) showReminder:(NSString *)reminderId;
- (void) refreshCollectionView;
@end
