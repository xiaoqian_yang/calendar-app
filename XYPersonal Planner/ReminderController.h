//
//  newDiaryController.h
//  XYDiary
//
//  Created by Xiaoqian Yang on 30/10/2014.
//  Copyright (c) 2014 XiaoqianYang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

static NSString *XYPersonalPlannerRemindID = @"com.XYPersonPlanner.RemindID";

@class Reminder;
@interface ReminderController : UIViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UITextField *remindTitle;
@property (strong, nonatomic) IBOutlet UITextView *message;
@property (strong, nonatomic) IBOutlet UIButton *mainImage;

- (IBAction)cancel:(id)sender;
- (IBAction)save:(id)sender;

- (IBAction)imagePressed:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *dateView;
@property (strong, nonatomic) IBOutlet UIButton *buttonComplete;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)cancelDatePicker:(id)sender;
- (IBAction)confirmDatePicker:(id)sender;
- (IBAction)setComplete:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *locationTextField;
- (IBAction)locationInputDone:(id)sender;

@property (strong, nonatomic) Reminder * Reminder;
@property (strong, nonatomic) NSDate *remindTime;
@property (strong,nonatomic) CLLocation * cllocation;
@property (strong, nonatomic) NSString * reminderId;
@property (strong, nonatomic) NSString * reminderType;
@property (strong, nonatomic) NSString * location;
@property (nonatomic) double latitude;
@property (nonatomic) double longtitude;


@end
