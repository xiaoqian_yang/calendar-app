//
//  ReminderCell.h
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 21/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reminder.h"

@interface ReminderCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *remindTime;
@property (strong, nonatomic) IBOutlet UIButton *buttonRound;
@property (strong, nonatomic) IBOutlet UILabel *reminderTitle;
//@property (strong, nonatomic) IBOutlet UILabel *note;
//@property (strong, nonatomic) IBOutlet UIImageView *photo;
- (void) configCell:(Reminder *)reminder;
@end
