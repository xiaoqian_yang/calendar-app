//
//  Reminder.m
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 23/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import "Reminder.h"


@implementation Reminder

@dynamic message;
@dynamic picture;
@dynamic reminderType;
@dynamic time;
@dynamic title;
@dynamic reminderid;
@dynamic status;
@dynamic locationName;
@dynamic latitude;
@dynamic longitude;

- (NSString*)month {
    if ([self.reminderType isEqual:XYPersonalPlannerRemindTypeTime]) {
        NSDateFormatter * fomatter = [[NSDateFormatter alloc] init];
        fomatter.dateFormat = @"MMM-YYYY";
        NSDate * date = [NSDate dateWithTimeIntervalSince1970:self.time];
        return [fomatter stringFromDate:date];
    }
    else {
        return @"Location Reminder";
    }
}

@end
