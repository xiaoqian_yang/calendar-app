//
//  ReminderCell.m
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 21/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import "ReminderCell.h"

@implementation ReminderCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ReminderCell" owner:self options:nil];
        
        if (arrayOfViews.count < 1) {
            return nil;
        }

        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]){
            return nil;
        }

        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}

/*
- (CGFloat) heightForEntry:(Reminder *)reminder {
    const float topMargin = 10.0f;
    const float bottomMargin = 10.0f;
    const float maxHeight = 81.0f;
    
    UIFont * font = self.note.font;
    CGRect bondingBox = [reminder.message boundingRectWithSize:CGSizeMake(207, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    
    return  MIN(maxHeight, topMargin + bottomMargin + bondingBox.size.height);
}*/

- (void) configCell:(Reminder *)reminder {
    [self.buttonRound setEnabled:NO];
    self.buttonRound.layer.cornerRadius = 4;
    if ([reminder.reminderType isEqual:XYPersonalPlannerRemindTypeTime]) {
        NSDateFormatter * fomatter = [[NSDateFormatter alloc] init];
        fomatter.dateStyle = kCFDateFormatterShortStyle;
        fomatter.timeStyle = kCFDateFormatterShortStyle;
        fomatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_AU"];
        [self.remindTime setText:[fomatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:reminder.time]]];
    }
    else {
        [self.remindTime setText:reminder.locationName];
    }
    
    self.reminderTitle.text = reminder.title;
    /*
    self.note.text = reminder.message;
    self.note.lineBreakMode = UILineBreakModeWordWrap;
    self.note.numberOfLines = 0;
    self.note.frame = CGRectMake(self.note.frame.origin.x, self.note.frame.origin.y, self.note.frame.size.width, [self heightForEntry:reminder]);
    
    if (reminder.picture != nil) {
        CGRect rect = self.photo.frame;
        self.photo.image = [UIImage imageWithData:reminder.picture];
        self.photo.contentMode = UIViewContentModeScaleAspectFill;
        self.photo.frame = rect;
        [self.photo setAlpha:0.5];
    }
    else {
        self.photo.image = [UIImage imageNamed:@"backgroundCalender"];
    }*/
}

# pragma Gesture recognizer

- (void) handleTap {
   UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Like" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 1);
    dispatch_after(time, dispatch_get_main_queue(), ^{
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
}

@end
