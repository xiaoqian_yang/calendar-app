//
//  RemindPopUpView.h
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 22/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Reminder;
@class ReminderViewController;
@interface RemindPopUpView : UIView
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *note;
@property (strong, nonatomic) IBOutlet UIImageView *photo;

@property (strong, nonatomic) Reminder* reminder;

- (void) configView:(Reminder*)reminder superController:(ReminderViewController*)superController;

- (IBAction)edit:(id)sender;
- (IBAction)finish:(id)sender;

@property (strong, nonatomic) ReminderViewController * superController;
@end
