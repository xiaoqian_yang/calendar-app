//
//  CollectionHeaderView.m
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 22/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import "CollectionHeaderView.h"

@implementation CollectionHeaderView
- (void)configHeader:(NSString*) title
{
    self.sectionTitle.text = title;
    //self.backgroundColor = [UIColor colorWithRed:250/255.0 green:240/255.0 blue:230/255.0 alpha:1.0];
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
}
@end
