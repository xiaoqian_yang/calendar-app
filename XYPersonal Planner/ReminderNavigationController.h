//
//  ReminderNavigationController.h
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 22/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReminderNavigationController : UINavigationController

@end
