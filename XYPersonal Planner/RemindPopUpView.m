//
//  RemindPopUpView.m
//  XYPersonal Planner
//
//  Created by Xiaoqian Yang on 22/02/2015.
//  Copyright (c) 2015 XiaoqianYang. All rights reserved.
//

#import "RemindPopUpView.h"
#import "Reminder.h"
#import "XYCoreDataStack.h"
#import "ReminderViewController.h"

@implementation RemindPopUpView

- (id)init {
    self = [super init];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"RemindPopUpView" owner:self options:nil];
        
        if (arrayOfViews.count < 1) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"RemindPopUpView" owner:self options:nil];
        
        if (arrayOfViews.count < 1) {
            return nil;
        }
                
        self = [arrayOfViews objectAtIndex:0];
        [self setBounds:frame];
    }
    return self;
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.frame = bounds;
}

- (CGFloat) heightForEntry:(Reminder *)reminder {
    const float topMargin = 10.0f;
    const float bottomMargin = 10.0f;
    const float maxHeight = 81.0f;
    
    UIFont * font = self.note.font;
    CGRect bondingBox = [reminder.message boundingRectWithSize:CGSizeMake(207, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    
    return  MIN(maxHeight, topMargin + bottomMargin + bondingBox.size.height);
}

- (void) configView:(Reminder*)reminder superController:(ReminderViewController*)superController {
    self.layer.cornerRadius = 30;
    self.layer.borderColor = [[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1] CGColor];
    self.layer.borderWidth = 1;
    self.reminder = reminder;
    self.superController = superController;

    self.title.text = reminder.title;
    
    self.note.text = reminder.message;
    self.note.lineBreakMode = UILineBreakModeWordWrap;
    self.note.numberOfLines = 0;
    self.note.frame = CGRectMake(self.note.frame.origin.x, self.note.frame.origin.y, self.note.frame.size.width, [self heightForEntry:reminder]);
    
    if (reminder.picture != nil) {
        self.photo.image = [UIImage imageWithData:reminder.picture];
        self.photo.contentMode = UIViewContentModeScaleAspectFill;
        [self.photo setAlpha:0.5];
    }
    else {
        self.photo.image = [UIImage imageNamed:@"backgroundCalender"];
    }

}

- (IBAction)edit:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)finish:(id)sender {
    XYCoreDataStack * coreDataStack = [XYCoreDataStack defaultStack];
    self.reminder.status = XYPersonalPlannerRemindStatusFinished;
    [coreDataStack saveContext];
    
    [self.superController refreshCollectionView];
    
    [self removeFromSuperview];
}


@end
